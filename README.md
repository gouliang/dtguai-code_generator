**项目说明** 

前端地址：https://gitee.com/gouliang/dtguai-vue   
代码生成器源码: https://gitee.com/gouliang/dtguai-code_generator  
web-api地址:https://gitee.com/gouliang/dtguai-web  
app-api地址:https://gitee.com/gouliang/dtguai-app-api  

#更新记录

###2019年4月24日11:26:00   
model  增加链式编程方式    
@Accessors(chain = true)    


###2019年4月17日17:19:52 
oracle 解决一个用户下 多个库   
user_col_comments  -> all_col_comments 类似语句更换   




####entity、xml、dao、service、html、js、sql代码，减少70%以上的开发任务
<br> 

多数据库id编号  

mysql:10xx   
  
oracle:20xx    

postgresql:30xx  
  
sqlserver:40xx  
   
dm7:50xx  


##多数据源 需要根据规定的id来创建下拉列表  
操作步骤:  
1.src\main\resources\views\generator.html 添加下拉列表 注意:id必须按照上面数据库编号添加   
  
2.src\main\resources\db.properties 添加对应数据源   
  
3.src\main\java\com\paoge\generator\db\DynamicDataSourceConfiguration.java 添加数据源  

注:dm7 一定要保证表名单一不重复( 后续优化) 切勿在多模式下建同样命名表  
这样是为了通用性好  
 
  
