package com.paoge.generator.utils;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.paoge.generator.entity.ColumnEntity;
import com.paoge.generator.entity.TableEntity;

/**
 * 代码生成器   工具类
 */
public class GenUtils {

	public static List<String> getTemplates() {
		List<String> templates = new ArrayList<String>();

		// web
		templates.add("template/modules/form/Form.java.vm");
		templates.add("template/modules/form/AddForm.java.vm");
		templates.add("template/modules/controller/Controller.java.vm");
		templates.add("template/modules/service/Service.java.vm");
		templates.add("template/modules/service/impl/ServiceImpl.java.vm");
		templates.add("template/modules/dao/Dao.java.vm");
		templates.add("template/modules/model/Model.java.vm");
		templates.add("template/modules/mapper/Mapper.xml.vm");

		templates.add("template/sql/menu.sql.vm");

		templates.add("template/vue/index.vue.vm");
		templates.add("template/vue/add-or-update.vue.vm");

		// app
		templates.add("template/app/form/Form.java.vm");
		templates.add("template/app/form/AddForm.java.vm");
		templates.add("template/app/api/Controller.java.vm");
		templates.add("template/app/service/Service.java.vm");
		templates.add("template/app/service/impl/ServiceImpl.java.vm");
		templates.add("template/app/dao/Dao.java.vm");
		templates.add("template/app/model/Model.java.vm");
		templates.add("template/app/mapper/Mapper.xml.vm");

		return templates;
	}

	/**
	 * 生成代码
	 */
	public static void generatorCode(Map<String, String> table, List<Map<String, String>> columns,
			ZipOutputStream zip) {
		// 配置信息
		Configuration config = getConfig();
		boolean hasBigDecimal = false;
		// 表信息
		TableEntity tableEntity = new TableEntity();
		tableEntity.setTableName(table.get("tableName"));
		tableEntity.setComments(table.get("tableComment"));
		// 表名转换成Java类名
		String className = tableToJava(tableEntity.getTableName(), config.getString("tablePrefix"));
		tableEntity.setClassName(className);
		tableEntity.setClassname(StringUtils.uncapitalize(className));

		// 列信息
		List<ColumnEntity> columsList = new ArrayList<>();
		for (Map<String, String> column : columns) {
			ColumnEntity columnEntity = new ColumnEntity();
			columnEntity.setColumnName(column.get("columnName"));
			columnEntity.setDataType(column.get("dataType"));
			columnEntity.setComments(column.get("columnComment"));
			columnEntity.setExtra(column.get("extra"));

			// 列名转换成Java属性名
			String attrName = columnToJava(columnEntity.getColumnName());
			columnEntity.setAttrName(attrName);
			columnEntity.setAttrname(StringUtils.uncapitalize(attrName));

			// 列的数据类型，转换成Java类型
			String attrType = config.getString(columnEntity.getDataType(), "unknowType");
			columnEntity.setAttrType(attrType);
			if (!hasBigDecimal && attrType.equals("BigDecimal")) {
				hasBigDecimal = true;
			}
			// 是否主键
			if ("PRI".equalsIgnoreCase(column.get("columnKey")) && tableEntity.getPk() == null) {
				tableEntity.setPk(columnEntity);
			}

			columsList.add(columnEntity);
		}
		tableEntity.setColumns(columsList);

		// 没主键，则第一个字段为主键
		if (tableEntity.getPk() == null) {
			tableEntity.setPk(tableEntity.getColumns().get(0));
		}

		// 设置velocity资源加载器
		Properties prop = new Properties();
		prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.init(prop);
		String mainPath = config.getString("mainPath");
		// 封装模板数据
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", tableEntity.getTableName());
		map.put("comments", tableEntity.getComments());
		map.put("pk", tableEntity.getPk());
		map.put("className", tableEntity.getClassName());
		map.put("classname", tableEntity.getClassname());
		map.put("pathName", tableEntity.getClassname().toLowerCase());
		map.put("columns", tableEntity.getColumns());
		map.put("hasBigDecimal", hasBigDecimal);
		map.put("mainPath", mainPath);
		map.put("package", config.getString("package"));
		map.put("appPackage", config.getString("appPackage"));
		map.put("moduleName", config.getString("moduleName"));
		map.put("author", config.getString("author"));
		map.put("email", config.getString("email"));
		map.put("datetime", DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN));
		VelocityContext context = new VelocityContext(map);

		// 获取模板列表
		List<String> templates = getTemplates();
		for (String template : templates) {
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);

			try {
				// 添加到zip
				zip.putNextEntry(new ZipEntry(getFileName(template, tableEntity, config.getString("package"),
						config.getString("moduleName"), config.getString("appPackage"))));
				IOUtils.write(sw.toString(), zip, "UTF-8");
				IOUtils.closeQuietly(sw);
				zip.closeEntry();
			} catch (IOException e) {
				throw new RRException("渲染模板失败，表名：" + tableEntity.getTableName(), e);
			}
		}
	}

	/**
	 * 列名转换成Java属性名
	 */
	public static String columnToJava(String columnName) {
		return WordUtils.capitalizeFully(columnName, new char[] { '_' }).replace("_", "");
	}

	/**
	 * 表名转换成Java类名
	 */
	public static String tableToJava(String tableName, String tablePrefix) {
		if (StringUtils.isNotBlank(tablePrefix)) {
			tableName = tableName.replace(tablePrefix, "");
		}
		return columnToJava(tableName);
	}

	/**
	 * 获取配置信息
	 */
	public static Configuration getConfig() {
		try {
			return new PropertiesConfiguration("generator.properties");
		} catch (ConfigurationException e) {
			throw new RRException("获取配置文件失败，", e);
		}
	}

	/**
	 * 获取文件名
	 */
	public static String getFileName(String template, TableEntity table, String packageName, String moduleName,
			String appPackageName) {
		String packagePath = "main" + File.separator + "java" + File.separator;
		String appPackagePath = "main" + File.separator + "java" + File.separator;
		if (StringUtils.isNotBlank(packageName)) {
			packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
		}

		// app-----------------------------------
		if (StringUtils.isNotBlank(appPackageName)) {
			appPackagePath += appPackageName.replace(".", File.separator) + File.separator;
		}

		// app----------------------------------------------------------
		if (template.equals("template/app/form/AddForm.java.vm")) {
			return "app" + File.separator + appPackagePath + "api" + File.separator + "form" + File.separator
					+ table.getClassname() + File.separator + "Add" + table.getClassName() + "Form.java";
		}

		if (template.equals("template/app/form/Form.java.vm")) {
			return "app" + File.separator + appPackagePath + "api" + File.separator + "form" + File.separator
					+ table.getClassname() + File.separator + table.getClassName() + "Form.java";
		}

		if (template.equals("template/app/api/Controller.java.vm")) {
			return "app" + File.separator + appPackagePath + "api" + File.separator + "controller" + File.separator
					+ table.getClassname() + File.separator + table.getClassName() + "Api.java";
		}

		if (template.equals("template/app/service/Service.java.vm")) {
			return "app" + File.separator + appPackagePath + "service" + File.separator + table.getClassName()
					+ "Service.java";
		}

		if (template.equals("template/app/service/impl/ServiceImpl.java.vm")) {
			return "app" + File.separator + appPackagePath + "service" + File.separator + "impl" + File.separator
					+ table.getClassName() + "ServiceImpl.java";
		}

		if (template.equals("template/app/dao/Dao.java.vm")) {
			return "app" + File.separator + appPackagePath + "dao" + File.separator + table.getClassName() + "Dao.java";
		}

		if (template.equals("template/app/model/Model.java.vm")) {
			return "app" + File.separator + appPackagePath + "model" + File.separator + table.getClassName() + ".java";
		}

		if (template.equals("template/app/mapper/Mapper.xml.vm")) {
			return "app" + File.separator + "main" + File.separator + "resources" + File.separator + "mapper"
					+ File.separator + "api" + File.separator + table.getClassName() + "Mapper.xml";
		}

		// web------------------------------------------------------
		if (template.equals("template/modules/form/AddForm.java.vm")) {
			return "web" + File.separator + packagePath + "form" + File.separator + table.getClassname()
					+ File.separator + "Add" + table.getClassName() + "Form.java";
		}

		if (template.equals("template/modules/form/Form.java.vm")) {
			return "web" + File.separator + packagePath + "form" + File.separator + table.getClassname()
					+ File.separator + table.getClassName() + "Form.java";
		}

		if (template.equals("template/modules/controller/Controller.java.vm")) {
			return "web" + File.separator + packagePath + "controller" + File.separator + table.getClassname()
					+ File.separator + table.getClassName() + "Controller.java";
		}

		if (template.equals("template/modules/service/Service.java.vm")) {
			return "web" + File.separator + packagePath + "service" + File.separator + table.getClassName()
					+ "Service.java";
		}

		if (template.equals("template/modules/service/impl/ServiceImpl.java.vm")) {
			return "web" + File.separator + packagePath + "service" + File.separator + "impl" + File.separator
					+ table.getClassName() + "ServiceImpl.java";
		}

		if (template.equals("template/modules/dao/Dao.java.vm")) {
			return "web" + File.separator + packagePath + "dao" + File.separator + table.getClassName() + "Dao.java";
		}

		if (template.equals("template/modules/model/Model.java.vm")) {
			return "web" + File.separator + packagePath + "model" + File.separator + table.getClassName() + ".java";
		}

		if (template.equals("template/modules/mapper/Mapper.xml.vm")) {
			return "web" + File.separator + "main" + File.separator + "resources" + File.separator + "mapper"
					+ File.separator + "web" + File.separator + moduleName + File.separator + table.getClassName()
					+ "Mapper.xml";
		}

		// sql
		if (template.equals("template/sql/menu.sql.vm")) {
			return "sql" + File.separator + table.getClassName().toLowerCase() + "_menu.sql";
		}

		// vue
		if (template.equals("template/vue/index.vue.vm")) {
			return "vue" + File.separator + "src" + File.separator + "views" + File.separator + "modules"
					+ File.separator + moduleName + File.separator + table.getClassName() + ".vue";
		}

		if (template.equals("template/vue/add-or-update.vue.vm")) {
			return "vue" + File.separator + "src" + File.separator + "views" + File.separator + "modules"
					+ File.separator + moduleName + File.separator + table.getClassName() + "-add-or-update.vue";
		}

		return null;
	}
}
