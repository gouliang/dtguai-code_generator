package com.paoge.generator.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import com.paoge.generator.db.DynamicDataSourceContextHolder;
import com.paoge.generator.service.SysGeneratorService;
import com.paoge.generator.utils.PageUtils;
import com.paoge.generator.utils.Query;
import com.paoge.generator.utils.R;

@Controller
@RequestMapping("/sys/generator")
public class SysGeneratorController {
	@Autowired
	private SysGeneratorService sysGeneratorService;

	/**
	 * 列表
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params, Integer id) {

		if (null != id) {
			if(id<2000) {
				SysGeneratorService.dbType="mysql";
			}else if(id<3000) {
				SysGeneratorService.dbType="oracle";
			}else if(id<4000){
				SysGeneratorService.dbType="postgresql";
			}else if(id<5000){
				SysGeneratorService.dbType="sqlserver";
			}else if(id<6000){
				SysGeneratorService.dbType="dm7";
			}
			DynamicDataSourceContextHolder.setSlave(id);
		} else {
			return null;
		}

		// 查询列表数据
		PageUtils pageUtil = sysGeneratorService.queryList(new Query(params));

		return R.ok().put("page", pageUtil);

	}

	/**
	 * table info
	 */
	@ResponseBody
	@RequestMapping("/tableInfo")
	public R tableInfo(String tableName, Integer id) {
		DynamicDataSourceContextHolder.setSlave(id);
		// 查询列信息
		List<Map<String, String>> columns = sysGeneratorService.queryColumns(tableName);
		System.out.println(JSON.toJSON(columns));
		// 查询列表数据
		String jsondata = "{" + "    \"page\":\"1\"," + "    \"total\":1," + "    \"records\":\"1\"," + "    \"rows\":["
				+ "      {" + "        \"id\":\"1\"," + "        \"cell\":[\"1\",\"item 13\",\"1.00\"]" + "      }"
				+ "    ]" + "  }";
		return R.ok().put("jsondata", jsondata);
	}

	/**
	 * 选择数据库以后展示所有列表
	 */
	@ResponseBody
	@RequestMapping("/dbInfo")
	public R dbInfo(Integer id) {
		// System.out.println("id:" + id);
		if (null != id) {
			DynamicDataSourceContextHolder.setSlave(id);
		} else {
			return null;
		}
		// 查询列表数据

		PageUtils pageUtil = sysGeneratorService.queryList(new Query(new HashMap<String, Object>(), false));

		return R.ok().put("page", pageUtil);
	}

	/**
	 * 部分生成代码
	 */
	@RequestMapping("/code")
	public void code(String tables, HttpServletResponse response, Integer id) throws IOException {

		DynamicDataSourceContextHolder.setSlave(id);

		byte[] data = sysGeneratorService.generatorCode(tables.split(","));

		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\"project.zip\"");
		response.addHeader("Content-Length", "" + data.length);
		response.setContentType("application/octet-stream; charset=UTF-8");

		IOUtils.write(data, response.getOutputStream());
	}
}
