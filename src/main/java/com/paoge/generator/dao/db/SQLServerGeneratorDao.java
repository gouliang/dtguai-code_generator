package com.paoge.generator.dao.db;

import org.apache.ibatis.annotations.Mapper;

/**
 * SQLServer代码生成器
 */
@Mapper
public interface SQLServerGeneratorDao extends GeneratorDao {

}
