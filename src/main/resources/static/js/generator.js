$(function() {
	$("#jqGrid").jqGrid({
		url : 'sys/generator/list',
		datatype : "json",
		colModel : [
			{
				label : '表名',
				name : 'tableName',
				width : 100,
				key : true
			},
			{
				label : 'Engine',
				name : 'engine',
				width : 70
			},
			{
				label : '表备注',
				name : 'tableComment',
				width : 100
			},
			{
				label : '创建时间',
				name : 'createTime',
				width : 100
			}
		],
		viewrecords : true,
		height : 385,
		rowNum : 10,
		rowList : [ 10, 30, 50, 100, 200 ],
		rownumbers : true,
		rownumWidth : 25,
		autowidth : true,
		multiselect : true,
		pager : "#jqGridPager",
		jsonReader : {
			root : "page.list",
			page : "page.currPage",
			total : "page.totalPage",
			records : "page.totalCount"
		},
		prmNames : {
			page : "page",
			rows : "limit",
			order : "order"
		},
		subGrid : true,
		subGridUrl : 'sys/generator/tableInfo?tableName=user&id=3',
        subGridModel : [ {
          name : [ '字段名', '类型', '说明' ],
          width : [ 55, 200, 80 ]
        } ],
		gridComplete : function() {
			//隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
				"overflow-x" : "hidden"
			});
		}
	});
});

var vm = new Vue({
	el : '#rrapp',
	data : {
		q : {
			tableName : null
		}
	},
	methods : {
		query : function() {
			var id = $('#select_id').val();

			if (id == null) {
				alert("请选择数据库");
				return;
			}
			$("#jqGrid").jqGrid('setGridParam', {
				postData : {
					'tableName' : vm.q.tableName,
					'id' : id
				},
				page : 1
			}).trigger("reloadGrid");
		},
		
		generator : function() {
			var id = $('#select_id').val();
			if (id == null) {
				alert("请选择数据库");
				return;
			}		
			var tableNames = getSelectedRows();
			if (tableNames == null) {
				return;
			}
			location.href = "sys/generator/code?tables=" + tableNames.join()+"&id="+id;
		},
	}
});