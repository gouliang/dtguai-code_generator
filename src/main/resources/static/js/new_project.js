$(function() {
	$("#jqGrid").jqGrid({
		url : 'sys/generator/dbInfo',
		datatype : "json",
		colModel : [
			{
				label : '表名',
				name : 'tableName',
				width : 100,
				key : true
			},
			{
				label : 'Engine',
				name : 'engine',
				width : 70
			},
			{
				label : '表备注',
				name : 'tableComment',
				width : 100
			},
			{
				label : '创建时间',
				name : 'createTime',
				width : 100
			}
		],
		viewrecords : true,
		height : 585,
		rowNum : 0,
		rownumbers : true,
		rownumWidth : 25,
		autowidth : true,
		multiselect : false,
		jsonReader : {
			root : "page.list",
			page : "page.currPage",
			total : "page.totalPage",
			records : "page.totalCount"
		},

		gridComplete : function() {
			//隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({
				"overflow-x" : "hidden"
			});
		}
	});
});



var vm = new Vue({
	el : '#rrapp',
	data : {
		q : {
			tableName : null
		}
	},
	methods : {
		query : function() {
			$("#jqGrid").jqGrid('setGridParam', {
				postData : {
					'id' : $('#select_id').val()
				},
			}).trigger("reloadGrid");
		},
		generator_all : function() {
			var id = $('#select_id').val();
            if(id == null){
            	alert("请选择数据库");
                return ;
            }
			//选数据库
			location.href = "sys/generator/project?id=" + id;
		}
	}
});